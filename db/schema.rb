# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200321012100) do

  create_table "emotion_records", force: :cascade do |t|
    t.datetime "created_at"
    t.integer  "emotion"
    t.integer  "intencity"
    t.string   "reason"
    t.integer  "user_id"
    t.index ["id"], name: "index_emotion_records_on_id"
    t.index ["user_id"], name: "index_emotion_records_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.integer  "chat_id"
    t.integer  "age"
    t.string   "gender"
    t.boolean  "active",             default: true
    t.integer  "analytics_interval"
    t.datetime "reminder_time"
    t.string   "role"
    t.string   "feedback"
    t.boolean  "notifications",      default: false
    t.index ["id"], name: "index_users_on_id"
  end

end
