class CreateEmotionRecords < ActiveRecord::Migration[5.0]
  def up
    create_table :emotion_records do |t|
      t.datetime :created_at
      t.integer :emotion
      t.integer :intencity
      t.string :reason
      t.belongs_to :user
    end
  end

  def down
    drop_table :emotion_records
  end
end