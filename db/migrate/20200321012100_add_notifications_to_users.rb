class AddNotificationsToUsers < ActiveRecord::Migration[5.0]
  def up
    change_table :users do |t|
      t.boolean :notifications, :default => false
    end
  end

  def down
    change_table :users do |t|
      t.remove :notifications
    end
  end
end
