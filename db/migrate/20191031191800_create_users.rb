class CreateUsers < ActiveRecord::Migration[5.0]
  def up
    create_table :users do |t|
      t.integer :chat_id
      t.integer :age
      t.string :gender
      t.boolean :active, :default => true
      t.integer :analytics_interval
      t.datetime :reminder_time
    end
  end

  def down
    drop_table :users
  end
end