class AddRoleToUsers < ActiveRecord::Migration[5.0]
  def up
    change_table :users do |t|
      t.string :role, :default => nil
    end
  end

  def down
    change_table :users do |t|
      t.remove :role
    end
  end
end
