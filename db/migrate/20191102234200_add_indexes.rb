class AddIndexes < ActiveRecord::Migration[5.0]
  def change
    add_index :users, :id
    add_index :emotion_records, :id
  end
end