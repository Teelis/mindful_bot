class AddFeedbackToUsers < ActiveRecord::Migration[5.0]
  def up
    change_table :users do |t|
      t.string :feedback, :default => nil
    end
  end

  def down
    change_table :users do |t|
      t.remove :feedback
    end
  end
end
