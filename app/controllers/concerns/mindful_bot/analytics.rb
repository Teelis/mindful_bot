require 'prawn'
require 'squid'

module MindfulBot
  module Analytics

    COLORS = %w(b50000 ff8000 f2b91d 65c900 007537 00cfcf 004aab 000080 8000ff 870087 ff0080).freeze

    # Public: создание pdf-файла с аналитикой
    #
    # options - Hash
    #           :name       - имя файла
    #           :data       - записи EmotionRecord, из которых создавать файл
    #           :start_date - начало периода аналитики
    #           :end_date   - конец периода аналитики
    #
    # Returns nothing
    def create_pdf(options)
      pdf = Prawn::Document.new
      pdf.font_families.update(
        "Verdana" => {
          :bold => Rails.root.join("app/assets/fonts/Verdana_bold.ttf"),
          :normal => Rails.root.join("app/assets/fonts/Verdana_regular.ttf")
        }
      )
      pdf.font "Verdana"
      pdf_options = {
        pdf: pdf,
        data: options[:data],
        start_date: options[:start_date],
        end_date: options[:end_date]
      }
      create_table(pdf_options)
      pdf.move_down 20
      pdf.start_new_page if pdf.cursor < 320
      create_chart(pdf_options)
      pdf.render_file Rails.root.join("tmp/#{options[:name]}")
    end

    private

    # Internal: формирование таблицы с аналитикой
    #
    # options - Hash
    #           :pdf        - генерируемый pdf-файл
    #           :data       - записи EmotionRecord, из которых создавать файл
    #           :start_date - начало периода аналитики
    #           :end_date   - конец периода аналитики
    #
    # Returns nothing
    def create_table(options)
      header = I18n.t('telegram_webhooks.analytics.table.header',
                      start_date: options[:start_date].strftime('%_d.%_m'),
                      end_date: options[:end_date].strftime('%_d.%_m'))
      options[:pdf].text(header, :style => :bold, :align => :center)
      options[:pdf].move_down 20

      rows = [I18n.t('telegram_webhooks.analytics.table.header_row')]

      options[:data].each do |record|
        date = record.created_at.strftime('%_d.%_m')
        emotion = EmotionRecord.emotion_to_str(record.emotion)[0...-2]
        rows << [date, emotion, record.intencity, record.reason]
      end

      options[:pdf].table(rows, :header => true, :column_widths => {0 => 45, 1 => 90, 2 => 105, 3 => 300})
    end

    # Internal: формирование графика с аналитикой
    #
    # options - Hash
    #           :pdf        - генерируемый pdf-файл
    #           :data       - записи EmotionRecord, из которых создавать файл
    #           :start_date - начало периода аналитики
    #           :end_date   - конец периода аналитики
    #
    # Returns nothing
    def create_chart(options)
      header = I18n.t('telegram_webhooks.analytics.chart.header')
      options[:pdf].text(header, :style => :bold, :align => :center)
      options[:pdf].move_down 30

      records = options[:data].
        select("emotion, count(emotion) as frequency, avg(intencity) as avg_intencity").
        group(:emotion)

      y_max = records.max_by { |e| e[:avg_intencity] }[:avg_intencity]

      options[:pdf].chart(prepare_chart_data(records), type: :point, steps: y_max.ceil, colors: COLORS)

      x_label = I18n.t('telegram_webhooks.analytics.chart.x')
      y_label = I18n.t('telegram_webhooks.analytics.chart.y')

      options[:pdf].text_box(y_label,
                             :at => [-40, options[:pdf].cursor + 150],
                             :width => 100,
                             :height => 60,
                             :rotate => 90,
                             :rotate_around => :center)

      options[:pdf].text x_label, :align => :right
    end

    def prepare_chart_data(records)
      x_max = records.max_by { |e| e[:frequency] }[:frequency]

      records.each_with_object({}) do |record, chart_data|
        emotion = EmotionRecord.emotion_to_str(record.emotion)[0...-2]
        chart_data[emotion] = Hash.new
        (1..x_max).each do |i|
          chart_data[emotion][i] = nil
          chart_data[emotion][i] = record.avg_intencity if i == record.frequency
        end
      end
    end
  end
end
