module MindfulBot
  module Tools
    INTERVALS = HashWithIndifferentAccess.new(
      7 => 'раз в неделю',
      14 => 'раз в 2 недели',
      30 => 'раз в месяц'
    ).freeze

    TIME_REGEX = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/.freeze

    def interval_int_to_str(interval_int)
      INTERVALS[interval_int]
    end

    def interval_str_to_int(interval_str)
      INTERVALS.key(interval_str)
    end

    # Public: удаление предлога из интервала аналитики ('раз в неделю' -> 'неделю')
    #
    # options - interval_int - ключ интервала
    #
    # Returns интервал в винительном падеже
    def interval_without_preposition(interval_int)
      INTERVALS[interval_int][6..-1]
    end

    def parse_time(input_time)
      return false unless TIME_REGEX =~ input_time

      Time.zone.parse(payload['text'])
    end
  end
end
