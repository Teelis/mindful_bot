module InstanceMethods
  extend ActiveSupport::Concern

  def save_age(*)
    age = payload['text']
    chat_id = payload['chat']['id']

    if age != t('.survey.age.option.unspecified')
      user = User.find_by(chat_id: chat_id)
      unless user.update(age: age)
        save_context :save_age
        bot.send_message chat_id: chat_id, text: I18n.t('.telegram_webhooks.input_error')
        return
      end
    end

    ask_gender(chat_id)
  end

  def save_gender(*)
    chat_id = payload['chat']['id']
    user = User.find_by(chat_id: chat_id)

    gender = payload['text']
    case gender
    when t('.survey.gender.options.female')
      user.update(gender: 'female')
    when t('.survey.gender.options.male')
      user.update(gender: 'male')
    end

    ask_reminder_time(chat_id)
  end

  def save_reminder_time(*)
    chat_id = payload['chat']['id']
    user = User.find_by(chat_id: chat_id)
    input_time = payload['text']

    reminder_time = parse_time(input_time)

    unless reminder_time
      save_context :save_reminder_time
      bot.send_message chat_id: chat_id, text: I18n.t('.telegram_webhooks.input_error')
      return
    end

    Resque.remove_delayed(EmotionQuestionJob, user.chat_id, true) unless session[:new_user]

    user.update(reminder_time: reminder_time)

    Resque.enqueue_at(user.time_for_job, EmotionQuestionJob, chat_id, true)
    bot.send_message(
      chat_id: chat_id,
      text: t('.reminder_time.confirmation', time: user.reminder_time.to_s(:time))
    )
    ask_analytics_interval(chat_id) if user.analytics_interval.blank?
  end

  def save_analytics_interval(*)
    chat_id = payload['chat']['id']
    user = User.find_by(chat_id: chat_id)

    interval = interval_str_to_int(payload['text'])

    unless interval
      input_error(:save_analytics_interval, [I18n.t('.telegram_webhooks.analytics_interval.options')])
      return
    end

    if user.analytics_interval
      Resque.remove_delayed(AnalyticsSenderJob, user.chat_id, user.analytics_interval)

      sender_options = {
        chat_id: user.chat_id,
        end_date: Time.zone.now,
        start_date: Time.zone.now - user.analytics_interval.days,
        message: t('telegram_webhooks.analytics.message')
      }

      analytics_sender = ::MindfulBot::AnalyticsSender.new(sender_options)
      analytics_sender.call
    end

    user.update(analytics_interval: interval)
    Resque.enqueue_in(interval.day, AnalyticsSenderJob, user.chat_id, interval)
    interval_str = interval_int_to_str(user.analytics_interval)
    response = [t('.analytics_interval.confirmation', interval: interval_str)].join

    if session[:new_user]
      Resque.enqueue_in(user.analytics_interval.days + 1.hour, SurveyJob, user.chat_id)
      session.delete(:new_user)
      response << "\n"
      response << t('.start.goodbye')
    end

    bot.send_message(
      chat_id: chat_id,
      text: response,
      reply_markup: {remove_keyboard: true}
    )
  end

  def save_emotion(*)
    chat_id = payload['chat']['id']
    user = User.find_by(chat_id: chat_id)
    emotion = EmotionRecord.emotion_to_i(payload['text'])

    unless emotion
      input_error(:save_emotion, t('.add_record.emotion.options').each_slice(3).to_a)
      return
    end

    record = EmotionRecord.create(user_id: user.id, emotion: emotion)
    session[:record_id] = record.id
    ask_emotion_intencity(chat_id)
  end

  def save_emotion_intencity(*)
    chat_id = payload['chat']['id']
    intencity = payload['text']
    record = EmotionRecord.find_by(id: session[:record_id])

    if I18n.t('.telegram_webhooks.add_record.intencity.options').include?(intencity)
      record.update(intencity: intencity.to_i)
      ask_emotion_reason(chat_id)
    else
      input_error(:save_emotion_intencity,
                  I18n.t('.telegram_webhooks.add_record.intencity.options').each_slice(5).to_a)
    end
  end

  def save_emotion_reason(*)
    reason = payload['text'].to_s
    record_id = session[:record_id]
    session.delete(:record_id)
    record = EmotionRecord.find_by(id: record_id)

    record.update(reason: reason)
    respond_with :message, text: t('.add_record.finish')
    ask_for_next_record
  end

  def another_record_confirmation(*)
    answer = payload['text']

    if answer == t('.add_record.add_more.options.add')
      ask_emotion(payload['chat']['id'])
    else
      respond_with(:message,
                   text: I18n.t('.telegram_webhooks.add_record.add_more.goodbye'),
                   reply_markup: {remove_keyboard: true})
    end
  end

  def input_error(context, options)
    save_context context
    respond_with(:message,
                 text: t('.choiсe_error'),
                 reply_markup: {
                   keyboard: options,
                   resize_keyboard: true,
                   one_time_keyboard: true
                 })
  end

  private

  def ask_age(chat_id)
    start_survey = [
      I18n.t('.telegram_webhooks.survey.start_question'),
      "\n",
      I18n.t('.telegram_webhooks.survey.age.question')
    ].join

    save_context :save_age
    bot.send_message(chat_id: chat_id,
                     text: start_survey,
                     reply_markup: {
                       keyboard: [t('.survey.age.option').values],
                       resize_keyboard: true,
                       one_time_keyboard: true
                     })
  end

  def ask_gender(chat_id)
    save_context :save_gender
    bot.send_message(chat_id: chat_id,
                     text: I18n.t('.telegram_webhooks.survey.gender.question'),
                     reply_markup: {
                       keyboard: [t('.survey.gender.options').values],
                       resize_keyboard: true,
                       one_time_keyboard: true
                     })
  end

  def ask_reminder_time(chat_id)
    save_context :save_reminder_time
    bot.send_message(chat_id: chat_id,
                     text: I18n.t('.telegram_webhooks.reminder_time.question'),
                     reply_markup: {remove_keyboard: true})
  end

  def ask_analytics_interval(chat_id)
    save_context :save_analytics_interval
    bot.send_message(chat_id: chat_id,
                     text: I18n.t('.telegram_webhooks.analytics_interval.question'),
                     reply_markup: {
                       keyboard: [I18n.t('.telegram_webhooks.analytics_interval.options')],
                       resize_keyboard: true,
                       one_time_keyboard: true
                     })
  end

  def ask_emotion(*)
    emotions = t('.add_record.emotion.options').each_slice(3).to_a

    save_context :save_emotion
    respond_with(:message,
                 text: t('telegram_webhooks.add_record.emotion.question'),
                 reply_markup: {
                   keyboard: emotions,
                   resize_keyboard: true,
                   one_time_keyboard: true
                 })
  end

  def ask_emotion_intencity(chat_id)
    save_context :save_emotion_intencity
    intencities = I18n.t('.telegram_webhooks.add_record.intencity.options').each_slice(5).to_a
    bot.send_message(chat_id: chat_id,
                     text: I18n.t('.telegram_webhooks.add_record.intencity.question'),
                     reply_markup: {
                       keyboard: intencities,
                       resize_keyboard: true,
                       one_time_keyboard: true
                     })
  end

  def ask_emotion_reason(chat_id)
    save_context :save_emotion_reason
    bot.send_message(chat_id: chat_id,
                     text: I18n.t('.telegram_webhooks.add_record.reason.question'),
                     reply_markup: {remove_keyboard: true})
  end

  def ask_for_next_record(*)
    save_context :another_record_confirmation
    respond_with(:message,
                 text: I18n.t('.telegram_webhooks.add_record.add_more.question'),
                 reply_markup: {
                   keyboard: [I18n.t('.telegram_webhooks.add_record.add_more.options').values],
                   resize_keyboard: true,
                   one_time_keyboard: true
                 })
  end
end
