class TelegramWebhooksController < Telegram::Bot::UpdatesController
  include Telegram::Bot::UpdatesController::MessageContext
  include Telegram::Bot::UpdatesController::Session
  include InstanceMethods
  include MindfulBot::Tools
  include MindfulBot::Analytics

  def stop!(*)
    chat_id = payload['chat']['id']
    user = User.find_by(chat_id: chat_id)

    return unless user.active?

    user.update(active: false)

    send_notification(:users_count_changed, sign: '-')

    Resque.remove_delayed(EmotionQuestionJob, user.chat_id, true)
    Resque.remove_delayed(EmotionQuestionJob, user.chat_id, false)
    Resque.remove_delayed(AnalyticsSenderJob, user.chat_id, user.analytics_interval)
    Resque.remove_delayed(SurveyJob, user.chat_id)
    respond_with :message, text: t('.stop')
    save_context :save_feedback
    bot.send_message(chat_id: chat_id,
                     text: I18n.t('.telegram_webhooks.cancellation_feedback.question'),
                     reply_markup: {
                       keyboard: [[I18n.t('.telegram_webhooks.cancellation_feedback.skip')]],
                       resize_keyboard: true,
                       one_time_keyboard: true
                     })
  end

  def start!(*)
    chat_id = payload['chat']['id']

    greeting = [
      t('.start.greeting'),
      "\n",
      t('.help.content'),
      "\n"
    ].join

    respond_with :message, text: greeting, reply_markup: {remove_keyboard: true}

    user = User.find_by(chat_id: chat_id)

    unless user
      register_user(chat_id)
      return
    end

    if user.reminder_time.blank? || user.analytics_interval.blank?
      ask_reminder_time(chat_id)
      return
    end

    activate_user(user) unless user.active?

    user_analytic_interval = interval_int_to_str(user.analytics_interval)
    confirmation = [
      t('.reminder_time.confirmation', time: user.reminder_time.to_s(:time)),
      "\n",
      t('.analytics_interval.confirmation', interval: user_analytic_interval)
    ].join

    bot.send_message chat_id: chat_id, text: confirmation
  end

  def when!(*)
    chat_id = payload['chat']['id']
    ask_reminder_time(chat_id)
  end

  def interval!(*)
    chat_id = payload['chat']['id']
    ask_analytics_interval(chat_id)
  end

  def add!(*)
    chat_id = payload['chat']['id']
    ask_emotion(chat_id)
  end

  def help!(*)
    respond_with :message, text: t('.help.content')
  end

  def message(message)
    command = message['text'].mb_chars.downcase.split[0].to_s
    chat_id = message['chat']['id']

    case command
    when 'стоп'
      stop!
    when 'старт'
      start!
    when 'когда'
      when!
    when 'частота'
      interval!
    when 'добавить'
      add!
    when 'отложить'
      redis.incr("delay_counter")
      Resque.enqueue_in(1.hour, EmotionQuestionJob, chat_id, false)
      respond_with(:message,
                   text: I18n.t('.telegram_webhooks.daily.confirmation.reply'),
                   reply_markup: {remove_keyboard: true})
    when "помощь"
      help!
    else
      action_missing
    end
  end

  def users_count!(*)
    admin_command(payload['chat']['id']) do
      respond_with(:message, text: User.where(active: true).count)
    end
  end

  def delays_count!(*)
    admin_command(payload['chat']['id']) do
      respond_with(:message, text: redis.get("delay_counter"))
    end
  end

  def toggle_notifications!(*)
    chat_id = payload['chat']['id']
    admin_command(chat_id) do
      user = User.find_by(chat_id: chat_id)
      user.update(notifications: !user.notifications)
      respond_with(:message,
                   text: I18n.t('.telegram_webhooks.notifications.toggle',
                                flag: user.notifications))
    end
  end

  def action_missing(*)
    response = [t('.command_error'),
                "\n\n",
                t('.help.content')].join
    respond_with(:message,
                 text: response,
                 reply_markup: {remove_keyboard: true})
  end

  def save_feedback(*)
    answer = payload['text']
    chat_id = payload['chat']['id']
    return if answer == I18n.t('.telegram_webhooks.cancellation_feedback.skip')

    user = User.find_by(chat_id: chat_id)
    user.update(feedback: answer)

    send_notification(:feedback, feedback: answer)

    bot.send_message(chat_id: chat_id,
                     text: I18n.t('.telegram_webhooks.cancellation_feedback.thanks'),
                     reply_markup: {remove_keyboard: true})
  end

  private

  def redis
    @redis ||= Redis.current
  end

  def register_user(chat_id)
    User.create(chat_id: chat_id)
    send_notification(:users_count_changed, sign: '+')
    session[:new_user] = true
    ask_age(chat_id)
  end

  def activate_user(user)
    user.update(active: true)
    send_notification(:users_count_changed, sign: '+')
    Resque.enqueue_at(user.time_for_job, EmotionQuestionJob, user.chat_id, true)
    Resque.enqueue_in(user.analytics_interval.days, AnalyticsSenderJob, user.chat_id, user.analytics_interval)
  end

  def admin_command(chat_id)
    raise ArgumentError unless block_given?

    if User.find_by(chat_id: chat_id).admin?
      yield
    else
      action_missing
    end
  end

  def send_notification(event, options = {})
    notification =
      if event == :feedback
        I18n.t('.telegram_webhooks.notifications.feedback',
               feedback: options[:feedback])
      else
        I18n.t('.telegram_webhooks.notifications.users_count_changed',
               sign: options[:sign],
               users_count: User.where(active: true).count)
      end

    User.where(notifications: true).to_a.each do |user|
      bot.send_message(chat_id: user.chat_id, text: notification)
    end
  end
end
