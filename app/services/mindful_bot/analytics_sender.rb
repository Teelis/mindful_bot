module MindfulBot
  class AnalyticsSender
    include MindfulBot::Analytics

    def initialize(options)
      @chat_id = options[:chat_id]
      @start_date = options[:start_date]
      @end_date = options[:end_date]
      @message = options[:message]
    end

    def call
      user = User.find_by(chat_id: @chat_id)

      records = EmotionRecord.where(user_id: user.id, created_at: @start_date..@end_date)
      return if records.empty?

      file_options = {
        name: I18n.t('telegram_webhooks.analytics.filename',
                     id: @chat_id,
                     date: Time.zone.now.strftime('%Y.%_m.%_d')),
        data: records,
        start_date: @start_date,
        end_date: @end_date
      }

      create_pdf(file_options)
      file = File.open(Rails.root.join('tmp', file_options[:name]))
      return unless file

      begin
        Telegram.bot.send_message(
          chat_id: @chat_id,
          text: @message,
          reply_markup: {remove_keyboard: true}
        )

        Telegram.bot.send_document(chat_id: @chat_id, document: file)
      ensure
        file&.close
      end
    end
  end
end
