class SurveyJob
  extend MindfulBot::Tools

  @queue = :default

  def self.perform(chat_id)
    user = User.find_by(chat_id: chat_id)

    text = I18n.t('.telegram_webhooks.feedback',
                  period: interval_without_preposition(user.analytics_interval))

    Telegram.bot.send_message(chat_id: chat_id.to_i, text: text)
  end
end
