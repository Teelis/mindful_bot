class AnalyticsSenderJob
  @queue = :default

  def self.perform(chat_id, period)
    message = [
      I18n.t('telegram_webhooks.greeting'),
      ' ',
      I18n.t('telegram_webhooks.analytics.message')
    ].join

    sender_options = {
      chat_id: chat_id,
      end_date: Time.zone.now,
      start_date: Time.zone.now - period.days,
      message: message
    }

    sender = ::MindfulBot::AnalyticsSender.new(sender_options)

    begin
      sender.call
    rescue Telegram::Bot::Forbidden
      u = User.find_by(chat_id: chat_id)
      u.update(active: false)
      return
    end

    Resque.enqueue_in(period.days, AnalyticsSenderJob, chat_id, period)
  end
end
