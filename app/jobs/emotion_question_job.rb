class EmotionQuestionJob
  @queue = :default

  def self.perform(chat_id, add_next)
    user = User.find_by(chat_id: chat_id)
    begin
      text = [
        I18n.t('.telegram_webhooks.greeting'),
        ' ',
        I18n.t('.telegram_webhooks.daily.confirmation.question')
      ].join

      Telegram.bot.send_message(chat_id: chat_id.to_i,
                                text: text,
                                reply_markup: {
                                  keyboard: [I18n.t('.telegram_webhooks.daily.confirmation.options')],
                                  resize_keyboard: true,
                                  one_time_keyboard: true
                                })
    rescue Telegram::Bot::Forbidden
      user.update(active: false)
      send_notification(:users_count_changed, sign: '-')
      return
    end

    Resque.enqueue_at(user.time_for_job, EmotionQuestionJob, chat_id, true) if add_next
  end
end
