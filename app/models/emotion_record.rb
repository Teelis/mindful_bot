class EmotionRecord < ActiveRecord::Base
  belongs_to :user

  def self.emotion_to_i(emotion)
    emotion_array = I18n.t('telegram_webhooks.add_record.emotion.options')
    emotion_array.index(emotion)
  end

  def self.emotion_to_str(index)
    emotion_array = I18n.t('telegram_webhooks.add_record.emotion.options')
    emotion_array[index]
  end
end