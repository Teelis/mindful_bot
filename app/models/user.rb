class User < ActiveRecord::Base
  enum gender: {
    male: 'male',
    female: 'female'
  }

  validates :age, numericality: {only_integer: true,
                                 greater_than: 0,
                                 allow_nil: true}

  def time_for_job
    hours = reminder_time.hour
    minutes = reminder_time.min
    time = Time.zone.now.change(hour: hours, min: minutes)
    time += 1.day if time < Time.zone.now
    time
  end

  def admin?
    role == 'admin'
  end
end
