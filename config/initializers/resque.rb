require 'resque-scheduler'
require 'resque/scheduler/server'
require 'resque-cleaner'

Resque.logger = Logger.new("#{Rails.root}/log/resque.log")
Resque.logger.level = Logger::DEBUG
Resque.logger.formatter = Resque::VeryVerboseFormatter.new

if Rails.env.production?
  Resque::Server.use(Rack::Auth::Basic) do |user, password|
    [user, password] == [ENV['RESQUE_WEB_HTTP_BASIC_AUTH_USER'], ENV['RESQUE_WEB_HTTP_BASIC_AUTH_PASSWORD']]
  end
end
