require 'redis'

redis = Redis.current

redis.set("delay_counter", 0) unless redis.get("delay_counter")
