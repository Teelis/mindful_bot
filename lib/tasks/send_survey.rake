desc "Send survey to existing users"
task :send_survey => :environment do
  logger = Logger.new(STDOUT).tap { |l| l.formatter = Logger::Formatter.new }

  User.where(active: true).find_each do |user|
    begin
      Telegram.bot.send_message(chat_id: user.chat_id,
                                text: I18n.t('.telegram_webhooks.one_time_survey'))
      logger.info "sent message to #{user.chat_id}"
    rescue Telegram::Bot::Forbidden
      u = User.find_by(chat_id: user.chat_id)
      u.update(active: false)
      logger.info "removed #{user.chat_id} from active users"
    end
  end
end
